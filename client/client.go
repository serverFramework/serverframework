package client

import (
	"net"
	"time"
)

type Client interface {
	String() string
	Exit()

	WLock()
	WUnlock()

	Write(data []byte) (int, error)
	Flush()

	GetIdentify() string
	GetID() int64
	GetHBInterval() time.Duration

	GetConnector() net.Conn

	GetAuth() bool // 是否登录成功/认证成功
	SetAuth(flag bool)
}

type Notifycator interface {
	Notify(uid int64, tpe string)
}
