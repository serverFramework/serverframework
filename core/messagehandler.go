package core

import (
	. "bitbucket.org/serverFramework/serverFramework/client"
	. "bitbucket.org/serverFramework/serverFramework/protocol"
)

type MsgHandler interface {
	ProcessMsg(p Protocol, client Client, msg *Message)
}

var msgHandle = make(map[string]MsgHandler)

func RegisterMsg(name string, adapter MsgHandler) {
	Info("register msg handler", name)

	if adapter == nil {
		Error("message handler register adapter is nil")
		return
	}

	if _, ok := msgHandle[name]; ok {
		Warn("the handler", name, "already registed")
		return
	}

	msgHandle[name] = adapter
}
