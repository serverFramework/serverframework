package core

import (
	"strings"

	. "bitbucket.org/serverFramework/serverFramework/client"
	. "bitbucket.org/serverFramework/serverFramework/protocol"
)

type EventHandler interface {
	ProcessEvent(p *Protocol, client *Client)
}

var eventsHandler = make(map[string]EventHandler)

func ResigerEvent(event string, handle EventHandler) {
	if handle == nil {
		Error("events register handle is nil")
		return
	}

	if _, ok := eventsHandler[strings.ToUpper(event)]; ok {
		Warn("the event", event, "already registed")
		return
	}

	eventsHandler[strings.ToUpper(event)] = handle
}
