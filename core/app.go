package core

import (
	"fmt"
	"net"
	"os"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/serverFramework/serverFramework/client"
	"bitbucket.org/serverFramework/serverFramework/utils"
)

const (
	// VERSION represent server framework version.
	VERSION = "0.0.1"

	// DEV is for develop
	DEV = "dev"
	// PROD is for production
	PROD = "prod"

	CONNECT    = "connect"
	DISCONNECT = "disconnect"
)

var (
	// ServerApp is an application instance
	ServerApp     *ServerCore
	notifyHandler = make(map[int64]client.Notifycator)
)

type ServerCore struct {
	sync.RWMutex

	clientIDSequence int64     // 64bit atomic vars need to be first for proper alignment on 32bit platforms
	MaxClients       int64     // the server max client cat connected
	allowClients     int64     // the server allow login clients or the server begin queue
	authClients      int64     // the clients that had authed
	connClients      int64     // the clients that connected
	startTime        time.Time // server start time
	tcpListener      net.Listener
	wg               utils.WaitGroupWrapper
}

func init() {
	ServerApp = New()
}

func New() *ServerCore {
	sc := &ServerCore{
		startTime: time.Now(),
	}

	return sc
}

func (sc *ServerCore) Run() {
	sc.MaxClients = SConfig.MaxClients
	sc.allowClients = SConfig.AllowClients

	addr := SConfig.TCPAddr
	if SConfig.TCPPort != 0 {
		addr = fmt.Sprintf("%s:%d", SConfig.TCPAddr, SConfig.TCPPort)
	}

	tcpListener, err := net.Listen("tcp", addr)
	if err != nil {
		ServerLogger.Error("listen [%s:%s] failed ->%s", SConfig.TCPAddr, SConfig.TCPPort, err)
		os.Exit(0)
	}

	sc.Lock()
	sc.tcpListener = tcpListener
	sc.Unlock()

	Info("server listen on", addr)

	handle := &AcceptorHandler{}
	// start accept routine
	sc.wg.Wrap(func() {
		HandleAccept(sc.tcpListener, handle)
	})

	// start process msg ??
	sc.wg.Wrap(func() {

	})

	sc.wg.Wait()
}

func (sc *ServerCore) Version(app string) {
	ServerLogger.Info("%s BASED ON SF v%s(built w/%s)", app, VERSION, runtime.Version())
}

func (sc *ServerCore) AuthClient(flag bool) {
	var del int64
	if flag {
		del = 1
	} else {
		del = -1
	}
	atomic.AddInt64(&sc.authClients, del)
}

func (sc *ServerCore) ConnectedClient(flag bool) {
	var del int64
	if flag {
		del = 1
	} else {
		del = -1
	}
	atomic.AddInt64(&sc.connClients, del)
}

func (sc *ServerCore) GetMaxClients() int64 {
	return atomic.LoadInt64(&sc.MaxClients)
}

func (sc *ServerCore) GetAllowClients() int64 {
	return atomic.LoadInt64(&sc.allowClients)
}

func (sc *ServerCore) GetAuthClients() int64 {
	return atomic.LoadInt64(&sc.authClients)
}

func (sc *ServerCore) RegisterNotify(uid int64, notify client.Notifycator) {
	if _, ok := notifyHandler[uid]; ok {
		Warn("the uid", uid, "already registed")
		return
	}

	notifyHandler[uid] = notify
}
